const API = require('./src/http/API')
module.exports = {
    //...
    devServer: {
        https: false,
        open: false,
        proxy: {
            [API.DEV_AGENT]: {
                // '/mall': {
                target: API.BASE_URL, //'http://116.63.80.218:8082/wx/',
                ws: true, // 是否跨域
                changeOrigin: true,
                pathRewrite: {
                    ['^' + API.DEV_AGENT]: ''
                }
            }
        }
    },
    //...
}
