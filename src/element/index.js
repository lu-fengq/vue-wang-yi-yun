import { Button} from 'element-ui'
import Container from 'element-ui'
 
import Progress from 'element-ui'
import Loading from 'element-ui'
import Tabs from 'element-ui'
import Input from 'element-ui'
import NavMenu from 'element-ui'
import Carousel from 'element-ui'
import Layout from 'element-ui'
import Image from 'element-ui'
import Slider from 'element-ui'
import Table from 'element-ui'
const element = {
  install: function (Vue) {
    Vue.use(Progress)
    Vue.use(Loading)
    Vue.use(Button)
    Vue.use(Container)
    Vue.use(Input)
    Vue.use(NavMenu)
    Vue.use(Tabs)
    Vue.use(Carousel)
    Vue.use(Layout)
    Vue.use(Image)
    Vue.use(Slider)
    Vue.use(Table)
  }
}
export default element