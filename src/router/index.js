// import { make } from 'core-js/core/object'
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import SongListDetails from "../views/SongListDetails.vue"
import search from "../views/Search.vue"
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    redirect: '/home',
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    redirect: '/home/recommendation',
    children: [
      {
        // 当 /user/:id/profile 匹配成功，
        // UserProfile 会被渲染在 User 的 <router-view> 中

        path: 'recommendation',
        component: () => import(/* webpackChunkName: "about" */ '../views/Recommendation.vue'),
        meta: {
          active: 0
        }
      },
      {
        // 专属定制
        // 当 /user/:id/profile 匹配成功，
        // UserProfile 会被渲染在 User 的 <router-view> 中
        path: 'exclusivecustomization',
        component: () => import(/* webpackChunkName: "about" */ '../views/Exclusivecustomization.vue'),
        meta: {
          active: 1
        }
      },
      {
        // 歌单
        // 当 /user/:id/profile 匹配成功，
        // UserProfile 会被渲染在 User 的 <router-view> 中
        path: 'songlist',
        component: () => import(/* webpackChunkName: "about" */ '../views/Songlist.vue'),
        meta: {
          active: 2
        }
      },
      {
        // 排行榜
        // 当 /user/:id/profile 匹配成功，
        // UserProfile 会被渲染在 User 的 <router-view> 中
        path: 'rankinglist',
        component: () => import(/* webpackChunkName: "about" */ '../views/Rankinglist.vue'),
        meta: {
          active: 3
        }
      },
      {
        //歌手
        // 当 /user/:id/profile 匹配成功，
        // UserProfile 会被渲染在 User 的 <router-view> 中
        path: 'singer',
        component: () => import(/* webpackChunkName: "about" */ '../views/Singer.vue'),
        meta: {
          active: 4
        }
      },
      {
        //最新音乐
        // 当 /user/:id/profile 匹配成功，
        // UserProfile 会被渲染在 User 的 <router-view> 中
        path: 'newest',
        component: () => import(/* webpackChunkName: "about" */ '../views/Newest.vue'),
        meta: {
          active: 5
        }
      },
      // {
      //   // 当 /user/:id/posts 匹配成功
      //   // UserPosts 会被渲染在 User 的 <router-view> 中
      //   path: 'posts',
      //   component: UserPosts
      // }
    ]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  //歌单详情
  {
    path: "/Songlistdetails",
    name: "Songlistdetails",
    component: SongListDetails,
    redirect: "/Songlistdetails/songlist",
    children: [
      {
        // 当 /user/:id/profile 匹配成功，
        // UserProfile 会被渲染在 User 的 <router-view> 中
        path: 'songlist',
        component: () => import(/* webpackChunkName: "about" */ '../components/Songlistdetails/songlist.vue')

      },
    ]
  },
  // 搜索
  {
    path: "/search",
    name: "search",
    component: search,
    redirect: "/search/geqlist",
    children: [
      {
        // 当 /user/:id/profile 匹配成功，
        // UserProfile 会被渲染在 User 的 <router-view> 中
        path: 'geqlist',
        component: () => import(/* webpackChunkName: "about" */ '../components/search/Geqlist.vue')

      },
    ]
  },
]

const router = new VueRouter({
  routes
})

export default router
