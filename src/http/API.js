const DEV_AGENT = '/mall'
const API = {
  DEV_AGENT: DEV_AGENT,
  //跨域配置代理名称
  //接口根目录
  BASE_URL: 'http://localhost:3000',
  //登录接口账号
  LOGIN_CELLPHONE: "/login/cellphone",
  //邮箱登录
  LOGIN: "/login",
  //退出登录
  LOGOUT: "/logout",
  //短信验证码/captcha/sent
  CAPTCHA_SENT: "/captcha/sent",
  //短信证码验证
  CAPTCHA_VERIFY: "/captcha/verify",
  //推荐(不需要登录的)
  PERSOBALIZED: "/personalized",
  //每日推荐歌单(需要登录的)
  RECOMMEND_RESOURCE: "/recommend/resource",
  //每日推荐歌曲
  RECOMMEND_SONGS: "/recommend/songs",
  //热门博客没有登录的时候
  DJ_HOT: "/dj/hot",
  //需要登录过后的今日哟选
  DJ_TODAY_PERFERED: "/dj/today/perfered",
  //登录的时候的傍目
  DJ_PROGRAM_TOPLIStT: "/dj/program/toplist",
  //歌单详情
  PLAYLIST_DETAIL: "/playlist/detail",
  //获取歌曲信息歌曲详情
  SONG_DETAIL: "/song/detail",
  //获取到当前的音乐连接/song/url
  SONG_URL: "/song/url",
  //喜欢的音乐
  LIKELIST: "/likelist",
  //默认搜索关键词
  SEARCH_DEFAULT: "/search/default",
  //热搜列表（简陋）
  SEARCH_HOT: "/search/hot",
  //热搜榜单
  SEARCH_HOT_DETAIL: "/search/hot/detail",
  //热门歌手
  TOP_ARTISTS: "/top/artists",
  //热门歌曲
  PLAYLIST_CATLIST: "/playlist/catlist",
  //歌曲评论
  COMMENT_MUSIC: "/comment/music",
  //获取歌曲详情 /song/detail`
  SONG_DETAIL: "/song/detail",
  // 获取到歌曲/lyric
  LYRIC: "/lyric",
  // 搜索
  SEARCH: "/search",
  // 多重匹配
  SEARCH_MULTIMATCH: "/search/multimatch",
  // 默认搜索关键
  SEARCH_DEFAULT: "/search/default",
  // 热门歌单


  TOP_PLAYLIST_HIGHQUALITY: "/top/playlist/highquality"

}

module.exports = API