import axios from 'axios'
import API from "../http/API.js";
import store from "../store/index";


console.log(store.state.token)

const instance = axios.create({
    timeout: 5000,
    // withCredentials: true, // 跨域请求时发送Cooki


}) //co

// // 请求拦截
instance.interceptors.request.use(
    config => {
        const token = store.state.token
        // console.log(config.headers)
        if (token) {
            // config.headers['X-Litemall-Token'] = token
            // config.headers.withCredentials=true
        }

        return config
    },
    err => {

        return Promise.reject(err)
    }
)




instance.interceptors.response.use(
    res => {
        // do something
        // console.log(res)
        if (res.status == 200 && res.data.errmsg !== '成功') {
            // Toast.fail(res.data.errmsg)

            return res.data
        } else {
            return res.data
        }
    },
    err => {


        return err
    }

)


//进行打包的时候解开注释
export function get(url, data) {
    return instance.get(`${API.BASE_URL}${url}`, {
        params: data,

    })
}
// axios#post(url[, data[, config]])

// axios.post('连接地址 path和query参数直接放里面', '是作为请求主体被发送的数据 body参数', {
//     timeout: 1000,
//     headers: 'object 发送的自定义请求头'
// })
export function post(url, data) {
    return instance.post(`${API.BASE_URL}${url}`, data,

    )
}





// export function get(url, data) {
//     return instance.get(`${API.DEV_AGENT}${url}`, {
//         params: data,

//     })
// }
// // axios#post(url[, data[, config]])

// // axios.post('连接地址 path和query参数直接放里面', '是作为请求主体被发送的数据 body参数', {
// //     timeout: 1000,
// //     headers: 'object 发送的自定义请求头'
// // })
// export function post(url, data) {
//     return instance.post(`${API.DEV_AGENT}${url}`, data,

//     )
// }
