import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    denglo: null,
    token: "",
    //歌曲信息推荐歌单里面进去的
    geqlebiao: [],
    //需要播放的歌曲
    bofang: [],
    shuax: true,
    // 显示隐藏歌词面板

    lyric: true,

    //下一首播放功能，用于记录下一首歌播放的时候进行拿数据
    TheFollowingPiece: "",

    //立刻播放功能
    Playimmediately: "",

    //添加播放列表
    addbofang: [],
    lyricdate:"",


    // 拖动进度条的时候发生的改变歌词滚动位置


      // 判断有没有托转行为
      instipzhuan:false,
      

  
  },
  mutations: {
    dengl(state, lof) {

      if (localStorage.getItem("User") != null) {
        state.denglo = true
      } else {
        state.denglo = false
      }
      console.log(state.denglo)

    },
    istoken(state, token) {
      state.token = token
    },
    geui(state, isgeqlebiao) {
      state.geqlebiao = isgeqlebiao
    },
    //拿到歌曲的单号
    geq(state,) {

      state.bofang = JSON.parse(localStorage.getItem("bofang"))
      console.log(state.bofang)
    },
    shuax(state, shu) {
      state.shuax = shu
      state.shuax = true
    },
    //下一首播放的歌曲
    onTheFollowingPiece(state, shu) {
      state.TheFollowingPiece = JSON.parse(localStorage.getItem("liTheFollowingPiece"))
    },
    onPlayimmediately(state,) {
      state.Playimmediately = JSON.parse(localStorage.getItem("Playimmediately"))
    },
    // 添加到列表里面去啊大大、、
    addPalay(state) {
      state.addbofang = JSON.parse(localStorage.getItem("Addbofang"))
    },
    onshwoLyric(state, isLyric) {
      state.lyric = isLyric
    },
    // 获取到播放时间
    onlyricdate(state,lyricdate){
      state.lyricdate=lyricdate
    },
    // 进度条
 
  },
  actions: {
  },
  modules: {
  }
})
