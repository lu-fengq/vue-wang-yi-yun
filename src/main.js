import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '../src/css/reset.css'
import element from './element/index'
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/base.css';
Vue.config.productionTip = false
Vue.prototype.$ELEMENT = { size: 'small', zIndex: 3000 };
import axios from 'axios'
import { get, post } from './http'
Vue.prototype.$get = get
Vue.prototype.$post = post
// Vue.prototype.$post = post
Vue.prototype.$axios = axios;
Vue.use(element)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
